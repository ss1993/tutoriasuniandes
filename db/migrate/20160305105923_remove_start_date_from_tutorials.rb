class RemoveStartDateFromTutorials < ActiveRecord::Migration
  def change
    remove_column :tutorials, :start_date, :datetime
  end
end
