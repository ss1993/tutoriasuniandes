class RemoveEndDateFromTutorials < ActiveRecord::Migration
  def change
    remove_column :tutorials, :end_date, :datetime
  end
end
