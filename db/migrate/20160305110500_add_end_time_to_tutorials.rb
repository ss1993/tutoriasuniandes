class AddEndTimeToTutorials < ActiveRecord::Migration
  def change
    add_column :tutorials, :end_time, :time
  end
end
