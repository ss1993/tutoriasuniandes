class Tutorial < ActiveRecord::Base
  belongs_to :tutor, class_name: 'Student'
  has_and_belongs_to_many :students, autosave: true

  validates :course, :location, :capacity,
            :date, :time, :end_time,:comments,
            :original_professor, :tutor_id, :status, presence: true
  validates :capacity, numericality: {greater_than_or_equal_to: 1, less_than_or_equal_to: 20}
  validates :status, inclusion: {in: %w(ACTIVA CANCELADA REALIZADA)}

  validate :validate_correct_times
  validate :validate_correct_date

  def validate_correct_times
    if time && end_time
      errors.add(:end_time, 'End time should be after start time') if end_time < time
    end
  end

  def validate_correct_date
    if date
      errors.add(:date, 'Date should be after today') if date <= Date.today
    end
  end

  def as_json(options = {})
    super (options.merge(include: [:students, :tutor], except: :tutor_id))
  end

end
