angular.module('tutoriasCEU')
    .controller('TutorialsCtrl', [
        '$scope',
        'tutorials',
        'uiCalendarConfig',
        'tutorialsPromise',
        function($scope, tutorials, uiCalendarConfig, tutorialsPromise){

            $scope.tutorials = tutorialsPromise;
            $scope.eventSources = [[]];

            $scope.init = function() {
                $scope.eventSources[0] = getEventsFromTutorials();
            }

            $scope.init();

            function getEventsFromTutorials() {

                var resp = [];

                for (var tutorial in $scope.tutorials) {

                    var currentTutorial = $scope.tutorials[tutorial];

                    var date = new Date(currentTutorial.date);
                    var time = new Date(currentTutorial.time);
                    var endTime = new Date(currentTutorial.end_time);


                    var startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
                        time.getHours(), time.getMinutes());

                    var endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
                        endTime.getHours(), endTime.getMinutes());

                    resp.push({
                        id: tutorial,
                        title: $scope.tutorials[tutorial].course,
                        start: startDate,
                        end: endDate
                    });
                }

                return resp;

            }

            $scope.alertOnEventClick = function( event, jsEvent, view){
                $scope.selectedTutorial = $scope.tutorials[event.id];
                $scope.selectedTutorial.date = new Date($scope.selectedTutorial.date);
                $scope.selectedTutorial.time = new Date($scope.selectedTutorial.time);
                $scope.selectedTutorial.end_time = new Date($scope.selectedTutorial.end_time);
            };

            $scope.uiConfig = {
                calendar:{
                    lang: 'es',
                    height: 450,
                    editable: true,
                    header:{
                        left: 'title',
                        center: '',
                        right: 'today prev,next'
                    },
                    eventClick: $scope.alertOnEventClick
                }
            };


        }])