angular.module('tutoriasCEU')
    .controller('AssignedTutorialsCtrl', [
        '$scope',
        'assignedTutorials',
        '$state',
        function($scope, assignedTutorials, $state){
            $scope.assignedTutorials = assignedTutorials;

            $scope.newTutorial = {};

            $scope.newTutorial.time = new Date();
            $scope.newTutorial.end_time = new Date();

            $scope.today = new Date();

            $scope.openDatePicker = function($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };

            $scope.dateOptions = {
                startingDay: 1,
            };

            $scope.create = function() {
                var ret = assignedTutorials.create($scope.newTutorial).success(function(data){
                    console.log("Data in succes controller! "+data);
                    $state.go('assigned_tutorials');
                });
                console.log(ret);
            }

        }]);

angular.module('tutoriasCEU')
    .directive('correctTime', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if (viewValue >= scope.newTutorial.end_time) {
                    ctrl.$setValidity('correctTimes', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('correctTimes', true);
                    return viewValue;
                }

            });
        }
    };
});

angular.module('tutoriasCEU')
    .directive('correctEndTime', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(viewValue) {
                    if (viewValue <= scope.newTutorial.time) {
                        ctrl.$setValidity('correctTimes', false);
                        return undefined;
                    } else {
                        ctrl.$setValidity('correctTimes', true);
                        return viewValue;
                    }

                });
            }
        };
    });